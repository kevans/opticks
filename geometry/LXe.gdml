<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<gdml xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://service-spi.web.cern.ch/service-spi/app/releases/GDML/schema/gdml.xsd">

  <define>
    <matrix coldim="2" name="REFLECTIVITY0x21d7b80" values="7e-06 1 7.14e-06 1"/>
    <matrix coldim="2" name="EFFICIENCY0x21d7c60" values="7e-06 0 7.14e-06 0"/>
    <matrix coldim="2" name="REFLECTIVITY0x21d6060" values="7e-06 1 7.14e-06 1"/>
    <matrix coldim="2" name="EFFICIENCY0x21d6140" values="7e-06 0 7.14e-06 0"/>
  </define>

  <materials>
    <material Z="13" name="Al0x1fa3b20" state="solid">
      <T unit="K" value="293.15"/>
      <MEE unit="eV" value="166"/>
      <D unit="g/cm3" value="2.7"/>
      <atom unit="g/mole" value="26.9815"/>
    </material>
    <isotope N="124" Z="54" name="Xe1240x1f98870">
      <atom unit="g/mole" value="123.906"/>
    </isotope>
    <isotope N="126" Z="54" name="Xe1260x1f988e0">
      <atom unit="g/mole" value="125.904"/>
    </isotope>
    <isotope N="128" Z="54" name="Xe1280x1f98970">
      <atom unit="g/mole" value="127.904"/>
    </isotope>
    <isotope N="129" Z="54" name="Xe1290x1f98a10">
      <atom unit="g/mole" value="128.905"/>
    </isotope>
    <isotope N="130" Z="54" name="Xe1300x1f98a80">
      <atom unit="g/mole" value="129.904"/>
    </isotope>
    <isotope N="131" Z="54" name="Xe1310x1f98ac0">
      <atom unit="g/mole" value="130.905"/>
    </isotope>
    <isotope N="132" Z="54" name="Xe1320x1f98b30">
      <atom unit="g/mole" value="131.904"/>
    </isotope>
    <isotope N="134" Z="54" name="Xe1340x1f98ba0">
      <atom unit="g/mole" value="133.905"/>
    </isotope>
    <isotope N="136" Z="54" name="Xe1360x1f987e0">
      <atom unit="g/mole" value="135.907"/>
    </isotope>
    <element name="Xe0x1f98580">
      <fraction n="0.0009" ref="Xe1240x1f98870"/>
      <fraction n="0.0009" ref="Xe1260x1f988e0"/>
      <fraction n="0.0192" ref="Xe1280x1f98970"/>
      <fraction n="0.2644" ref="Xe1290x1f98a10"/>
      <fraction n="0.0408" ref="Xe1300x1f98a80"/>
      <fraction n="0.2118" ref="Xe1310x1f98ac0"/>
      <fraction n="0.2689" ref="Xe1320x1f98b30"/>
      <fraction n="0.1044" ref="Xe1340x1f98ba0"/>
      <fraction n="0.0887" ref="Xe1360x1f987e0"/>
    </element>
    <material name="LXe0x1f982d0" state="solid">
      <T unit="K" value="293.15"/>
      <MEE unit="eV" value="482"/>
      <D unit="g/cm3" value="3.02"/>
      <fraction n="1" ref="Xe0x1f98580"/>
    </material>
    <isotope N="1" Z="1" name="H10x1fa4830">
      <atom unit="g/mole" value="1.00782503081372"/>
    </isotope>
    <isotope N="2" Z="1" name="H20x1fa48a0">
      <atom unit="g/mole" value="2.01410199966617"/>
    </isotope>
    <element name="H0x1fa45d0">
      <fraction n="0.999885" ref="H10x1fa4830"/>
      <fraction n="0.000115" ref="H20x1fa48a0"/>
    </element>
    <material name="Vacuum0x1fa44a0" state="gas">
      <T unit="K" value="0.1"/>
      <P unit="pascal" value="1e-19"/>
      <MEE unit="eV" value="19.2"/>
      <D unit="g/cm3" value="1e-25"/>
      <fraction n="1" ref="H0x1fa45d0"/>
    </material>
  </materials>

  <solids>
    <sphere aunit="deg" deltaphi="360" deltatheta="180" lunit="mm" name="sphere0x21d3c70" rmax="20" rmin="0" startphi="0" starttheta="0"/>
    <opticalsurface finish="0" model="1" name="SphereSurface0x21d7d60" type="0" value="1">
      <property name="REFLECTIVITY" ref="REFLECTIVITY0x21d7b80"/>
      <property name="EFFICIENCY" ref="EFFICIENCY0x21d7c60"/>
    </opticalsurface>
    <box lunit="mm" name="scint_box0x21d38d0" x="178" y="178" z="226"/>
    <box lunit="mm" name="housing_box0x21d3960" x="179.27" y="179.27" z="227.27"/>
    <opticalsurface finish="0" model="1" name="HousingSurface0x21d6260" type="0" value="1">
      <property name="REFLECTIVITY" ref="REFLECTIVITY0x21d6060"/>
      <property name="EFFICIENCY" ref="EFFICIENCY0x21d6140"/>
    </opticalsurface>
    <box lunit="mm" name="expHall_box0x21c8520" x="2357.27" y="2357.27" z="2453.27"/>
  </solids>

  <structure>
    <volume name="sphere_log0x21d3e30">
      <materialref ref="Al0x1fa3b20"/>
      <solidref ref="sphere0x21d3c70"/>
    </volume>
    <volume name="scint_log0x21d3a20">
      <materialref ref="LXe0x1f982d0"/>
      <solidref ref="scint_box0x21d38d0"/>
      <physvol name="sphere0x21d3f00">
        <volumeref ref="sphere_log0x21d3e30"/>
        <position name="sphere0x21d3f00_pos" unit="mm" x="50" y="50" z="50"/>
      </physvol>
    </volume>
    <volume name="housing_log0x21d3ae0">
      <materialref ref="Al0x1fa3b20"/>
      <solidref ref="housing_box0x21d3960"/>
      <physvol name="scintillator0x21d3bb0">
        <volumeref ref="scint_log0x21d3a20"/>
      </physvol>
    </volume>
    <volume name="expHall_log0x21c8c40">
      <materialref ref="Vacuum0x1fa44a0"/>
      <solidref ref="expHall_box0x21c8520"/>
      <physvol name="housing0x21c8620">
        <volumeref ref="housing_log0x21d3ae0"/>
      </physvol>
    </volume>
    <skinsurface name="sphere_surface0x21d9b70" surfaceproperty="SphereSurface0x21d7d60">
      <volumeref ref="sphere_log0x21d3e30"/>
    </skinsurface>
    <skinsurface name="photocath_surf0x21d9af0" surfaceproperty="HousingSurface0x21d6260">
      <volumeref ref="housing_log0x21d3ae0"/>
    </skinsurface>
  </structure>

  <setup name="Default" version="1.0">
    <world ref="expHall_log0x21c8c40"/>
  </setup>

</gdml>
