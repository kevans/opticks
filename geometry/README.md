`OKX4Test --deletegeocache --gdmlpath ~/opticks/geometry/rich1_new.gdml --x4balanceskip 74,90,94 --x4nudgeskip 857,867 --x4pointskip 74,867`   
`cd CSG_GGeo` or `cg`      
Add `export X4Solid_convertSphere_enable_phi_segment=1` to .opticks_config   
`./run.sh`   
`cd CSGOptiX`   
`opticks-build7` or `b7`   
`EYE=0.5,0,0.5 MOI=Rich1MasterWithSubtract0xcf514d0 ./cxr_view.sh`   
