#include "OPTICKS_LOG.hh"
#include "RichTbSimH.hh"
#include <iostream>
#include <fstream>
#include "G4TransportationManager.hh"
#include "G4Opticks.hh"
#include "U.hh"


int main( int argc, char** argv, const char* argforced )
{
    OPTICKS_LOG( argc, argv );

    RichTbSimH rich1( argc, argv, argforced );
    
    int events[] = {1, 10, 50, 100, 200, 500, 1000};
    double time[] = {0,0,0,0,0,0,0};
    for(int i=0; i<7; i++){
        time[i] = rich1.beamOn(events[i]);
    }
    std::ofstream time_file;
    const char* openvkey = "WITH_OPTICKS_mode";
    int op_mode = U::getenvint(openvkey, 0);
    if(op_mode){
    time_file.open("Opticks_time.txt");
    }
    const char* g4envkey = "WITH_GEANT4_mode";
    int g4_mode = U::getenvint(g4envkey, 0);
    if(g4_mode){
    time_file.open("Geant4_time.txt");
    }
    for(int i=0; i<7; i++){
        std::cout << events[i] << "\t" << time[i] << "\t";
        time_file << events[i] << "\t" << time[i] << std::endl;
    }
    std::cout <<  std::endl;
    rich1.Finalize();
    time_file.close();
    return 0;
}
