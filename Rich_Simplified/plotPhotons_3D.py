from math import sqrt
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from opticks.ana.hismask import HisMask

ox_op_result = np.load('/tmp/liyu/opticks/source/evt/g4live/natural/1/ox.npy')
ox_g4_result = np.load('/tmp/liyu/opticks/source/evt/g4live/natural/-1/ox.npy')

op_result = []; g4_result = []
op_photons = []; g4_photons = []
hm = HisMask()
for i in range(len(ox_op_result[:,3].view(np.int32))):
    if( "SD" in hm.label(ox_op_result[i,3,3].view(np.int32)) ):
        op_result.append( [ox_op_result[i,0,:],ox_op_result[i,1,:]] )
    op_photons.append( [ox_op_result[i,0,:],ox_op_result[i,1,:]] )
for i in range(len(ox_g4_result[:,3].view(np.int32))):
    if( "SD" in hm.label(ox_g4_result[i,3,3].view(np.int32)) ):
        g4_result.append( [ox_g4_result[i,0,:],ox_g4_result[i,1,:]] )
    g4_photons.append( [ox_g4_result[i,0,:],ox_g4_result[i,1,:]] )
g4_result = np.array(g4_result); op_result = np.array(op_result)
g4_photons = np.array(g4_photons); op_photons = np.array(op_photons)

fig = plt.figure(figsize=(10,10))
ax = fig.gca(projection='3d')
for i in range(op_photons.shape[0]):
    pos = op_photons[i,0]
    dire = op_photons[i,1]
    #mold = sqrt( dire[0]**2+dire[1]**2+dire[2]**2 )
    #ax.quiver(pos[0],pos[1],pos[2],dire[0]/mold+pos[0],dire[1]/mold+pos[1],dire[2]/mold+pos[2])
    ax.quiver(pos[0],pos[1],pos[2],dire[0],dire[1],dire[2],length=100,arrow_length_ratio=0.6,linewidths=1,colors='r')
#print(pos[0],pos[1],pos[2],dire[0]/mold+pos[0],dire[1]/mold+pos[1],dire[2]/mold+pos[2])
for i in range(g4_photons.shape[0]):
    pos = g4_photons[i,0]
    dire = g4_photons[i,1]
    #print(pos,dire)
    #mold = sqrt( dire[0]**2+dire[1]**2+dire[2]**2 )
    #ax.quiver(pos[0],pos[1],pos[2],dire[0]/mold+pos[0],dire[1]/mold+pos[1],dire[2]/mold+pos[2])
    ax.quiver(pos[0],pos[1],pos[2],dire[0],dire[1],dire[2],length=100,arrow_length_ratio=0.6,linewidths=1,colors='b')
print(g4_photons.shape, op_photons.shape)
print(g4_result.shape, op_result.shape)
ax.view_init(elev=0,azim=-180)
#ax.view_init(elev=0,azim=-270)
plt.title( "Opticks photons: {}, Geant4 photons: {}".format(str(op_photons.shape[0]),str(g4_photons.shape[0])) )
plt.xlabel('x')
plt.ylabel('y')
#plt.show()
plt.savefig("figures/plotPhotons.pdf")
plt.close()

plt.scatter(op_result[:,0,0],op_result[:,0,1],s=5,color='red',label='Opticks: '+str(op_result.shape[0])+' hits')
plt.scatter(g4_result[:,0,0],g4_result[:,0,1],s=5,color='blue',label='Geant4: '+str(g4_result.shape[0])+' hits')
plt.legend(loc='best')
plt.title("Hits collected by Opticks and Geant4")
plt.xlabel("x/mm")
plt.ylabel("y/mm")
plt.savefig("figures/draw_Hits_xy.pdf")
#plt.show()
plt.close()

plt.scatter(op_result[:,0,1],op_result[:,0,2],s=5,color='red',label='Opticks: '+str(op_result.shape[0])+' hits')
plt.scatter(g4_result[:,0,1],g4_result[:,0,2],s=5,color='blue',label='Geant4: '+str(g4_result.shape[0])+' hits')
plt.legend(loc='best')
plt.title("Hits collected by Opticks and Geant4")
plt.xlabel("y/mm")
plt.ylabel("z/mm")
plt.savefig("figures/draw_Hits_yz.pdf")
#plt.show()
plt.close()

opticks_hits = open("build/Opticks_hits_substraction.txt","r")
op_result = []
for line in opticks_hits:
    op_result.append( [float(s) for s in line.strip().split()] )
op_result = np.array(op_result)

g4_file = open("build/Geant4_hits.txt","r")
g4_result = []
for line in g4_file:
    temp = [float(s) for s in line.strip().split()]
    g4_result.append(temp)
g4_result = np.array(g4_result)

plt.scatter(g4_result[:,1],g4_result[:,2],s=5,color='blue',label='Geant4: '+str(g4_result.shape[0])+' hits')
plt.scatter(op_result[:,1],op_result[:,2],s=5,color='red',label='Opticks: '+str(op_result.shape[0])+' hits')
plt.legend(loc='best')

#plt.axis([0,1600,900,2300])
#ax = plt.gca()
#ax.invert_xaxis()
plt.title("Hits collected by Opticks and Geant4")
plt.xlabel("y")
plt.ylabel("z")

plt.savefig("figures/plotHits.pdf")
plt.show()

