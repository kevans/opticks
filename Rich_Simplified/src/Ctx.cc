/*
 * Copyright (c) 2019 Opticks Team. All Rights Reserved.
 *
 * This file is part of Opticks
 * (see https://bitbucket.org/simoncblyth/opticks).
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 * you may not use this file except in compliance with the License.  
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */


#include <sstream>
#include <iomanip>

#include "Ctx.hh"
#include "G4SystemOfUnits.hh"
//#define WITH_DUMP 1


#include "G4Opticks.hh"
#include "TrackInfo.hh"
#include "G4OpticksRecorder.hh"
#include "CTrackInfo.hh"
#include "PLOG.hh"


#include "G4Event.hh"
#include "G4Track.hh"
#include "G4Step.hh"
#include "G4OpticalPhoton.hh"

#include "G4ThreeVector.hh"
#include "G4Step.hh"
#include "G4StepPoint.hh"



// Ctx::Ctx(unsigned opticks_mode)
Ctx::Ctx()
    :
  //    _opticks_mode(opticks_mode),   
    _recorder(op_mode? new G4OpticksRecorder:nullptr)
{
}

// bool Ctx::gpu_propagate() const 
// {
//     return _opticks_mode & 1 ; 
// } 
// bool Ctx::cpu_propagate() const 
// {
//     return _opticks_mode & 2 ; 
// } 






void Ctx::setEvent(const G4Event* event)
{
    _event = event ; 
    _event_id = event->GetEventID() ; 
    //_event_total += 1 ;
    //_event_track_count = 0 ; 

    _track = NULL ; 
    _track_id = -1 ; 
    _record_id = -1 ; 
}

void Ctx::setTrack(const G4Track* track)
{
    _track = track ; 
    _track_id = track->GetTrackID() - 1 ;

    _step = NULL ; 
    _step_id = -1 ;  

    const G4DynamicParticle* dp = track->GetDynamicParticle() ; 
    assert( dp ) ;  
    const G4ParticleDefinition* particle = dp->GetParticleDefinition() ;
    assert( particle ) ;  

    _track_particle_name = particle->GetParticleName() ; 

    _track_optical = particle == G4OpticalPhoton::OpticalPhotonDefinition() ;

    _track_pdg_encoding = particle->GetPDGEncoding() ;

    if(_track_optical)
    {
        setTrackOptical(track);  
    }
    else
    {
      if(op_mode){
        unsigned num_gs = G4Opticks::Get()->getNumGensteps() ; 
        unsigned max_gs = G4Opticks::Get()->getMaxGensteps() ; // default of zero means no limit  
        bool kill = max_gs > 0 && num_gs >= max_gs ;   
#ifdef CTX_LOG
        G4cout 
            << "Ctx::setTrack"   
            << " _track_particle_name " << _track_particle_name 
            << " _track_id " << _track_id 
            << " _step_id " << _step_id 
            << " num_gs " << num_gs 
            << " max_gs " << max_gs 
            << " kill " << kill
            << G4endl
            ;  
#endif
        if(kill)
        {
            const_cast<G4Track*>(track)->SetTrackStatus(fStopAndKill);
        }

      }
    }
}

void Ctx::postTrack( const G4Track* track)
{
    if( const_cast<G4Track*>(track)->GetTrackStatus()==fStopAndKill ) {
	std::ofstream ofile;
	ofile.open("Geant4_Steps.txt", std::ios::app);
        //const G4Step* s = track->GetStep();
	//const G4ThreeVector& pos_start = s->GetPreStepPoint()->GetPosition();
	//const G4ThreeVector& pos_end = s->GetPostStepPoint()->GetPosition();
	//ofile << pos_start.x()/mm << std::setw(10) << pos_start.y()/mm << std::setw(10) << pos_start.z()/mm << std::endl;
	//ofile << pos_end.x()/mm << std::setw(10) << pos_end.y()/mm << std::setw(10) << pos_end.z()/mm << std::endl;
	const G4ThreeVector& pos = track->GetPosition();
	const G4double time = track->GetGlobalTime();
	const G4ThreeVector& dir = track->GetMomentumDirection();
	const G4double weight = track->GetWeight();
	const G4ThreeVector& pol = track->GetPolarization();
	//const G4double wavelength = CLHEP::h_Planck*CLHEP::c_light/track->GetKineticEnergy();
	const G4double wavelength = 0.*nm;
    	ofile << pos.x()/mm << " " << pos.y()/mm << " " << pos.z()/mm << " " << time/ns << " " 
    	      << dir.x()/mm << " " << dir.y()/mm << " " << dir.z()/mm << " " << weight << " " 
    	      << pol.x() << " " << pol.y() << " " << pos.z() << " " << wavelength/nm << " "
	      << track->GetDefinition()->GetPDGEncoding() << std::endl;
	ofile.close();
    }

    if(_track_optical)
    {
        postTrackOptical(track);  
    }
    else if(op_mode)
    {
#ifdef CTX_LOG
        G4cout 
            << "Ctx::postTrack"
            << " _track_particle_name : " << _track_particle_name
            << G4endl 
            ; 
#endif
    }
}


void Ctx::setTrackOptical(const G4Track* track)
{
    const_cast<G4Track*>(track)->UseGivenVelocity(true);
    if(op_mode) {
    TrackInfo* info=dynamic_cast<TrackInfo*>(track->GetUserInformation()); 
    assert(info) ; 
    _record_id = info->photon_record_id ;  
    //std::cout << "Ctx::setTrackOptical.setAlignIndex " << _record_id << std::endl ;
    G4Opticks::Get()->setAlignIndex(_record_id);
    }
}

void Ctx::postTrackOptical(const G4Track* track)
{
    if(op_mode) {
    TrackInfo* info=dynamic_cast<TrackInfo*>(track->GetUserInformation()); 
    assert(info) ; 
    assert( _record_id == info->photon_record_id ) ;  
    //std::cout << "Ctx::postTrackOptical " << _record_id << std::endl ;
    G4Opticks::Get()->setAlignIndex(-1);
    }
}



void Ctx::setStep(const G4Step* step)
{  

    assert( _track ) ; 

    
    const G4Track* track = _track ? _track : step->GetTrack() ; 
    // curious 10.4.2 getting setStep before setTrack ?

    _step = step ; 
    _step_id = track->GetCurrentStepNumber() - 1 ;

    _track_step_count += 1 ;
    
    const G4StepPoint* pre = _step->GetPreStepPoint() ;
    //const G4StepPoint* post = _step->GetPostStepPoint() ;

    if(_step_id == 0) _step_origin = pre->GetPosition();

    if(op_mode){
    if(!_track_optical)
    {
#ifdef CTX_LOG
        G4cout
            << "Ctx::setStep" 
            << " _step_id " << _step_id 
            << " num_gs " << G4Opticks::Get()->getNumGensteps() 
            << G4endl
            ; 
#endif 
    }

    }
}



std::string Ctx::Format(const G4ThreeVector& vec, const char* msg, unsigned int fwid)
{   
    std::stringstream ss ; 
    ss 
       << " " 
       << msg 
       << "[ "
       << std::fixed
       << std::setprecision(3)  
       << std::setw(fwid) << vec.x()
       << std::setw(fwid) << vec.y()
       << std::setw(fwid) << vec.z()
       << "] "
       ;
    return ss.str();
}

std::string Ctx::Format(const G4StepPoint* point, const char* msg )
{
    const G4ThreeVector& pos = point->GetPosition();
    const G4ThreeVector& dir = point->GetMomentumDirection();
    const G4ThreeVector& pol = point->GetPolarization();
    std::stringstream ss ; 
    ss 
       << " " 
       << msg 
       << Format(pos, "pos", 10)
       << Format(dir, "dir", 10)
       << Format(pol, "pol", 10)
       ;
    return ss.str();
}

std::string Ctx::Format(const G4Step* step, const char* msg )
{
    const G4StepPoint* pre = step->GetPreStepPoint() ;
    const G4StepPoint* post = step->GetPostStepPoint() ;
    std::stringstream ss ; 
    ss 
       << " " 
       << msg 
       << std::endl 
       << Format(pre,  "pre ")
       << std::endl 
       << Format(post, "post")
       ; 
    return ss.str();
}




