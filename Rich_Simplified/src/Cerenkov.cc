//
// ********************************************************************
// * DISCLAIMER                                                       *
// *                                                                  *
// * The following disclaimer summarizes all the specific disclaimers *
// * of contributors to this software. The specific disclaimers,which *
// * govern, are listed with their locations in:                      *
// *   http://cern.ch/geant4/license                                  *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.                                                             *
// *                                                                  *
// * This  code  implementation is the  intellectual property  of the *
// * GEANT4 collaboration.                                            *
// * By copying,  distributing  or modifying the Program (or any work *
// * based  on  the Program)  you indicate  your  acceptance of  this *
// * statement, and all its terms.                                    *
// ********************************************************************
//
//
// $Id: Cerenkov.cc,v 1.17 2004/12/10 18:49:57 gcosmo Exp $
// GEANT4 tag $Name: geant4-07-00 $
//
////////////////////////////////////////////////////////////////////////
// Cerenkov Radiation Class Implementation
////////////////////////////////////////////////////////////////////////
//
// File:        Cerenkov.cc 
// Description: Continuous Process -- Generation of Cerenkov Photons
// Version:     2.1
// Created:     1996-02-21  
// Author:      Juliet Armstrong
// Updated:     2001-09-17, migration of Materials to pure STL (mma) 
//              2000-11-12 by Peter Gumplinger
//              > add check on CerenkovAngleIntegrals->IsFilledVectorExist()
//              in method GetAverageNumberOfPhotons 
//              > and a test for MeanNumPhotons <= 0.0 in DoIt
//              2000-09-18 by Peter Gumplinger
//              > change: aSecondaryPosition=x0+rand*aStep.GetDeltaPosition();
//                        aSecondaryTrack->SetTouchable(0);
//              1999-10-29 by Peter Gumplinger
//              > change: == into <= in GetContinuousStepLimit
//              1997-08-08 by Peter Gumplinger
//              > add protection against /0
//              > G4MaterialPropertiesTable; new physics/tracking scheme
//
// mail:        gum@triumf.ca
// modifed  Se 1-3-2005 to extract some values to the usertrack for output.
//        renamed the class.
////////////////////////////////////////////////////////////////////////

#include "G4ios.hh"
#include "G4Poisson.hh"
#include "G4LossTableManager.hh"

#include "Cerenkov.hh"
#include "RichTbPhotonUserInfoAttach.hh"
#include "RichTbPhotonProdHisto.hh"

#ifdef WITH_OPTICKS
#include "G4Opticks.hh"
#include "TrackInfo.hh"    
// TrackInfo is a simple struct holding the photon_record_id integer
#endif

using namespace std;

/////////////////////////
// Class Implementation  
/////////////////////////

        //////////////
        // Operators
        //////////////

// Cerenkov::operator=(const Cerenkov &right)
// {
// }

        /////////////////
        // Constructors
        /////////////////

Cerenkov::Cerenkov(const G4String& processName, G4ProcessType type)
           : G4VProcess(processName, type) ,
            fTrackSecondariesFirst(false),
			fMaxBetaChange(0),
            fMaxPhotons(0)
{

    thePhysicsTable = NULL;

	if (verboseLevel>0) {
           G4cout << GetProcessName() << " is created " << G4endl;
	}
	
	BuildThePhysicsTable();
}

// Cerenkov::Cerenkov(const Cerenkov &right)
// {
// }

        ////////////////
        // Destructors
        ////////////////

Cerenkov::~Cerenkov() 
{
	if (thePhysicsTable != NULL) {
	   thePhysicsTable->clearAndDestroy();
           delete thePhysicsTable;
	}
}

        ////////////
        // Methods
        ////////////

// PostStepDoIt
// -------------
//
G4VParticleChange*
Cerenkov::PostStepDoIt(const G4Track& aTrack, const G4Step& aStep)

// This routine is called for each tracking Step of a charged particle
// in a radiator. A Poisson-distributed number of photons is generated
// according to the Cerenkov formula, distributed evenly along the track
// segment and uniformly azimuth w.r.t. the particle direction. The 
// parameters are then transformed into the Master Reference System, and 
// they are added to the particle change. 

{
	//////////////////////////////////////////////////////
	// Should we ensure that the material is dispersive?
	//////////////////////////////////////////////////////

        aParticleChange.Initialize(aTrack);

        const G4DynamicParticle* aParticle = aTrack.GetDynamicParticle();
        const G4Material* aMaterial = aTrack.GetMaterial();

	G4StepPoint* pPreStepPoint  = aStep.GetPreStepPoint();
	G4StepPoint* pPostStepPoint = aStep.GetPostStepPoint();

	G4ThreeVector x0 = pPreStepPoint->GetPosition();
        G4ThreeVector p0 = aStep.GetDeltaPosition().unit();
	G4double t0 = pPreStepPoint->GetGlobalTime();

        G4MaterialPropertiesTable* aMaterialPropertiesTable =
                               aMaterial->GetMaterialPropertiesTable();
        if (!aMaterialPropertiesTable) return pParticleChange;
	G4MaterialPropertyVector* Rindex = 
                aMaterialPropertiesTable->GetProperty("RINDEX"); 
        if (!Rindex) return pParticleChange;

        // particle charge
        G4double charge = aParticle->GetDefinition()->GetPDGCharge();

        // particle beta
        G4double beta = (pPreStepPoint ->GetBeta() +
			 pPostStepPoint->GetBeta())*0.5;

	G4double MeanNumberOfPhotons = 
                 GetAverageNumberOfPhotons(charge,beta,aMaterial,Rindex);

        if (MeanNumberOfPhotons <= 0.0) {

                // return unchanged particle and no secondaries

                aParticleChange.SetNumberOfSecondaries(0);
 
                return pParticleChange;

        }

        G4double step_length;
        step_length = aStep.GetStepLength();

	MeanNumberOfPhotons = MeanNumberOfPhotons * step_length;

	G4int NumPhotons = (G4int) G4Poisson(MeanNumberOfPhotons);

	

#ifdef WITH_OPTICKS
    G4cout 
         << "L4Cerenkov::PostStepDoIt"
         << " t0 " << t0
         << " x0 " << x0
         << " step_length " << step_length
         << " MeanNumberOfPhotons " << MeanNumberOfPhotons
         << " NumPhotons " << NumPhotons
         << G4endl
         ;
#endif



	if (NumPhotons <= 0) {

		// return unchanged particle and no secondaries  

		aParticleChange.SetNumberOfSecondaries(0);
		
                return pParticleChange;
	}

	////////////////////////////////////////////////////////////////

	aParticleChange.SetNumberOfSecondaries(NumPhotons);

        if (fTrackSecondariesFirst) {
           if (aTrack.GetTrackStatus() == fAlive )
                   aParticleChange.ProposeTrackStatus(fSuspend);
        }
	
	////////////////////////////////////////////////////////////////

	G4double Pmin = Rindex->GetMinLowEdgeEnergy();
	G4double Pmax = Rindex->GetMaxLowEdgeEnergy();
	G4double dp = Pmax - Pmin;

	G4double nMax = Rindex->GetMaxValue();

        G4double BetaInverse = 1./beta;

	G4double maxCos = BetaInverse / nMax; 
	G4double maxSin2 = (1.0 - maxCos) * (1.0 + maxCos);

        G4double beta1 = pPreStepPoint ->GetBeta();
        G4double beta2 = pPostStepPoint->GetBeta();

        G4double MeanNumberOfPhotons1 =
                     GetAverageNumberOfPhotons(charge,beta1,aMaterial,Rindex);
        G4double MeanNumberOfPhotons2 =
                     GetAverageNumberOfPhotons(charge,beta2,aMaterial,Rindex);


#ifdef WITH_OPTICKS
    unsigned opticks_photon_offset = 0 ; 
    {
        opticks_photon_offset = G4Opticks::Get()->getNumPhotons(); 
        // total number of photons for all gensteps collected before this one
        // within this OpticksEvent (potentially crossing multiple G4Event) 
          
        G4Opticks::Get()->collectGenstep_G4Cerenkov_1042(
             &aTrack, 
             &aStep, 
             NumPhotons,

             BetaInverse,
             Pmin,
             Pmax,
             maxCos,

             maxSin2,
             MeanNumberOfPhotons1,
             MeanNumberOfPhotons2
            );  
    }    
#endif


    // NB eventually the below CPU photon generation loop 
    //    will be skipped, it is kept for now to allow comparisons for validation

	for (G4int i = 0; i < NumPhotons; i++) {

		// Determine photon energy
#ifdef WITH_OPTICKS
        unsigned record_id = opticks_photon_offset+i ; 
        //std::cout << "(photon gen loop) " << record_id << std::endl ;
        G4Opticks::Get()->setAlignIndex(record_id); 
#endif

		G4double rand;
		G4double sampledEnergy, sampledRI; 
		G4double cosTheta, sin2Theta;
		
		// sample an energy

		do {
			rand = G4UniformRand();	
			sampledEnergy = Pmin + rand * dp; 
			sampledRI = Rindex->Value(sampledEnergy);
			cosTheta = BetaInverse / sampledRI;  

			sin2Theta = (1.0 - cosTheta)*(1.0 + cosTheta);
			rand = G4UniformRand();	

		  // Loop checking, 07-Aug-2015, Vladimir Ivanchenko
		} while (rand*maxSin2 > sin2Theta);

		// Generate random position of photon on cone surface 
		// defined by Theta 

		rand = G4UniformRand();

		G4double phi = CLHEP::twopi*rand;
		G4double sinPhi = std::sin(phi);
		G4double cosPhi = std::cos(phi);

		// calculate x,y, and z components of photon energy
		// (in coord system with primary particle direction 
		//  aligned with the z axis)

		G4double sinTheta = std::sqrt(sin2Theta); 
		G4double px = sinTheta*cosPhi;
		G4double py = sinTheta*sinPhi;
		G4double pz = cosTheta;

		// Create photon momentum direction vector 
		// The momentum direction is still with respect
	 	// to the coordinate system where the primary
		// particle direction is aligned with the z axis  

		G4ParticleMomentum photonMomentum(px, py, pz);

		// Rotate momentum direction back to global reference
		// system 

                photonMomentum.rotateUz(p0);

		// Determine polarization of new photon 

		G4double sx = cosTheta*cosPhi;
		G4double sy = cosTheta*sinPhi; 
		G4double sz = -sinTheta;

		G4ThreeVector photonPolarization(sx, sy, sz);

		// Rotate back to original coord system 

                photonPolarization.rotateUz(p0);
		
                // Generate a new photon:

                G4DynamicParticle* aCerenkovPhoton =
                  new G4DynamicParticle(G4OpticalPhoton::OpticalPhoton(), 
  					                 photonMomentum);
		aCerenkovPhoton->SetPolarization
				     (photonPolarization.x(),
				      photonPolarization.y(),
				      photonPolarization.z());

		aCerenkovPhoton->SetKineticEnergy(sampledEnergy);

                // Generate new G4Track object:

                G4double NumberOfPhotons, N;

                do {
                   rand = G4UniformRand();
                   NumberOfPhotons = MeanNumberOfPhotons1 - rand *
                                (MeanNumberOfPhotons1-MeanNumberOfPhotons2);
                   N = G4UniformRand() *
                       std::max(MeanNumberOfPhotons1,MeanNumberOfPhotons2);
		  // Loop checking, 07-Aug-2015, Vladimir Ivanchenko
                } while (N > NumberOfPhotons);

                G4double delta = rand * aStep.GetStepLength();

                G4double deltaTime = delta / (pPreStepPoint->GetVelocity()+
                                      rand*(pPostStepPoint->GetVelocity()-
                                            pPreStepPoint->GetVelocity())*0.5);

                G4double aSecondaryTime = t0 + deltaTime;

                G4ThreeVector aSecondaryPosition =
                                    x0 + rand * aStep.GetDeltaPosition();

		G4Track* aSecondaryTrack = 
		new G4Track(aCerenkovPhoton,aSecondaryTime,aSecondaryPosition);

                aSecondaryTrack->SetTouchableHandle(
                                 aStep.GetPreStepPoint()->GetTouchableHandle());

                aSecondaryTrack->SetParentID(aTrack.GetTrackID());

		aParticleChange.AddSecondary(aSecondaryTrack);


#ifdef WITH_OPTICKS
        aSecondaryTrack->SetUserInformation(new TrackInfo( record_id ) );
        G4Opticks::Get()->setAlignIndex(-1); 
#endif


	}  // CPU photon generation loop 

	if (verboseLevel>0) {
	   G4cout <<"L4Cerenkov::PostStepDoIt DONE -- NumberOfSecondaries = "
	          << aParticleChange.GetNumberOfSecondaries() << G4endl;
	}


#ifdef WITH_OPTICKS
       G4cout 
           << "L4Cerenkov::PostStepDoIt G4Opticks.collectSecondaryPhotons"
           << G4endl 
           ;
 
        G4Opticks::Get()->collectSecondaryPhotons(pParticleChange) ; 
#endif

        return pParticleChange;
}


// BuildThePhysicsTable for the Cerenkov process
// ---------------------------------------------
//

void Cerenkov::BuildThePhysicsTable()
{
	if (thePhysicsTable) return;

	const G4MaterialTable* theMaterialTable=
	 		       G4Material::GetMaterialTable();
	G4int numOfMaterials = G4Material::GetNumberOfMaterials();

	// create new physics table
	
	thePhysicsTable = new G4PhysicsTable(numOfMaterials);

	// loop for materials

	for (G4int i=0 ; i < numOfMaterials; i++)
	{
		G4PhysicsOrderedFreeVector* aPhysicsOrderedFreeVector =
					new G4PhysicsOrderedFreeVector();

		// Retrieve vector of refraction indices for the material
		// from the material's optical properties table 

		G4Material* aMaterial = (*theMaterialTable)[i];

		G4MaterialPropertiesTable* aMaterialPropertiesTable =
				aMaterial->GetMaterialPropertiesTable();

		if (aMaterialPropertiesTable) {

		   G4MaterialPropertyVector* theRefractionIndexVector = 
		    	   aMaterialPropertiesTable->GetProperty("CKVRNDX");

		   if (theRefractionIndexVector) {
		
		      // Retrieve the first refraction index in vector
		      // of (photon momentum, refraction index) pairs 

		     // theRefractionIndexVector->ResetIterator();
		     // ++(*theRefractionIndexVector);	// advance to 1st entry

          G4double currentRI = (*theRefractionIndexVector)[0];
		      //G4double currentRI = theRefractionIndexVector->
          //		   GetProperty();

		      if (currentRI > 1.0) {

			 // Create first (photon momentum, Cerenkov Integral)
			 // pair  

            // G4double currentPM = theRefractionIndexVector->
            //	 GetPhotonMomentum();
            //G4double currentPM = theRefractionIndexVector->
			 			// GetPhotonMomentum();

       G4double currentPM = theRefractionIndexVector->Energy(0);
			 G4double currentCAI = 0.0;

			 aPhysicsOrderedFreeVector->
			 	 InsertValues(currentPM , currentCAI);

			 // Set previous values to current ones prior to loop

			 G4double prevPM  = currentPM;
			 G4double prevCAI = currentCAI;
                	 G4double prevRI  = currentRI;

			 // loop over all (photon momentum, refraction index)
			 // pairs stored for this material

       for (size_t i = 1;i < theRefractionIndexVector->GetVectorLength(); i++)
                           //			 while(++(*theRefractionIndexVector))
			 {

            currentRI = (*theRefractionIndexVector)[i];
            currentPM = theRefractionIndexVector->Energy(i);
            //	currentRI=theRefractionIndexVector->	
            //	GetProperty();

            //	currentPM = theRefractionIndexVector->
            //		GetPhotonMomentum();

				currentCAI = 0.5*(1.0/(prevRI*prevRI) +
					          1.0/(currentRI*currentRI));

				currentCAI = prevCAI + 
					     (currentPM - prevPM) * currentCAI;

				aPhysicsOrderedFreeVector->
				    InsertValues(currentPM, currentCAI);

				prevPM  = currentPM;
				prevCAI = currentCAI;
				prevRI  = currentRI;
			 }

		      }
		   }
		}

	// The Cerenkov integral for a given material
	// will be inserted in thePhysicsTable
	// according to the position of the material in
	// the material table. 

	thePhysicsTable->insertAt(i,aPhysicsOrderedFreeVector); 

	}
}

// GetContinuousStepLimit
// ----------------------
//

G4double Cerenkov::GetMeanFreePath(const G4Track&,
                                           G4double,
                                           G4ForceCondition*)
{
        return 1.;
}

// GetAverageNumberOfPhotons
// -------------------------
// This routine computes the number of Cerenkov photons produced per
// GEANT-unit (millimeter) in the current medium. 
//             ^^^^^^^^^^

G4double 
Cerenkov::GetAverageNumberOfPhotons(const G4double charge,
                              const G4double beta, 
			      const G4Material* aMaterial,
			      G4MaterialPropertyVector* Rindex) const
{
	const G4double Rfact = 369.81/(CLHEP::eV * CLHEP::cm);

        if(beta <= 0.0)return 0.0;

        G4double BetaInverse = 1./beta;

	// Vectors used in computation of Cerenkov Angle Integral:
	// 	- Refraction Indices for the current material
	//	- new G4PhysicsOrderedFreeVector allocated to hold CAI's
 
	G4int materialIndex = aMaterial->GetIndex();

	// Retrieve the Cerenkov Angle Integrals for this material  

	G4PhysicsOrderedFreeVector* CerenkovAngleIntegrals =
	(G4PhysicsOrderedFreeVector*)((*thePhysicsTable)(materialIndex));

        if(!(CerenkovAngleIntegrals->IsFilledVectorExist()))return 0.0;

	// Min and Max photon energies 
	G4double Pmin = Rindex->GetMinLowEdgeEnergy();
	G4double Pmax = Rindex->GetMaxLowEdgeEnergy();

	// Min and Max Refraction Indices 
	G4double nMin = Rindex->GetMinValue();	
	G4double nMax = Rindex->GetMaxValue();

	// Max Cerenkov Angle Integral 
	G4double CAImax = CerenkovAngleIntegrals->GetMaxValue();

	G4double dp, ge;

	// If n(Pmax) < 1/Beta -- no photons generated 

	if (nMax < BetaInverse) {
		dp = 0;
		ge = 0;
	} 

	// otherwise if n(Pmin) >= 1/Beta -- photons generated  

	else if (nMin > BetaInverse) {
		dp = Pmax - Pmin;	
		ge = CAImax; 
	} 

	// If n(Pmin) < 1/Beta, and n(Pmax) >= 1/Beta, then
	// we need to find a P such that the value of n(P) == 1/Beta.
	// Interpolation is performed by the GetEnergy() and
	// Value() methods of the G4MaterialPropertiesTable and
	// the GetValue() method of G4PhysicsVector.  

	else {
		Pmin = Rindex->GetEnergy(BetaInverse);
		dp = Pmax - Pmin;

		// need boolean for current implementation of G4PhysicsVector
		// ==> being phased out
		G4bool isOutRange;
		G4double CAImin = CerenkovAngleIntegrals->
                                  GetValue(Pmin, isOutRange);
		ge = CAImax - CAImin;

#ifdef WITH_DUMP
		if (verboseLevel>0) {
			G4cout << "CAImin = " << CAImin << G4endl;
			G4cout << "ge = " << ge << G4endl;
		}
#endif
	}
	
	// Calculate number of photons 
	G4double NumPhotons = Rfact * charge/CLHEP::eplus * charge/CLHEP::eplus *
                                 (dp - ge * BetaInverse*BetaInverse);

	return NumPhotons;		
}

G4double Cerenkov::PostStepGetPhysicalInteractionLength(
                                           const G4Track& aTrack,
                                           G4double,
                                           G4ForceCondition* condition)
{
        *condition = NotForced;
        G4double StepLimit = DBL_MAX;

        const G4Material* aMaterial = aTrack.GetMaterial();
	G4int materialIndex = aMaterial->GetIndex();

	// If Physics Vector is not defined no Cerenkov photons
	//    this check avoid string comparison below
	if(!(*thePhysicsTable)[materialIndex]) { return StepLimit; }

#ifdef L4LOG
       G4cout 
             << "L4Cerenkov::PostStepGetPhysicalInteractionLength"
             << "OK.0 : table  "
             << " materialIndex " << materialIndex
             << G4endl
             ; 
#endif 
        const G4DynamicParticle* aParticle = aTrack.GetDynamicParticle();
        const G4MaterialCutsCouple* couple = aTrack.GetMaterialCutsCouple();

        G4double kineticEnergy = aParticle->GetKineticEnergy();
        const G4ParticleDefinition* particleType = aParticle->GetDefinition();
        G4double mass = particleType->GetPDGMass();

        // particle beta
        G4double beta = aParticle->GetTotalMomentum() /
	                aParticle->GetTotalEnergy();
        // particle gamma
        G4double gamma = aParticle->GetTotalEnergy()/mass;

        G4MaterialPropertiesTable* aMaterialPropertiesTable =
                            aMaterial->GetMaterialPropertiesTable();

        G4MaterialPropertyVector* Rindex = NULL;

        if (aMaterialPropertiesTable)
                     Rindex = aMaterialPropertiesTable->GetProperty("RINDEX");

        G4double nMax;
        if (Rindex) {
           nMax = Rindex->GetMaxValue();
        } else {
           return StepLimit;
        }


#ifdef L4LOG

       G4cout 
             << "L4Cerenkov::PostStepGetPhysicalInteractionLength"
             << "OK.1 : rindex "
             << " nMax " << nMax 
             << G4endl
             ; 
#endif 

        G4double BetaMin = 1./nMax;
        if ( BetaMin >= 1. ) return StepLimit;

#ifdef L4LOG
       G4cout 
             << "L4Cerenkov::PostStepGetPhysicalInteractionLength"
             << "OK.2 : BetaMin < 1 "
             << " BetaMin  " << BetaMin  
             << G4endl
             ; 
#endif 


        G4double GammaMin = 1./std::sqrt(1.-BetaMin*BetaMin);

        if (gamma < GammaMin ) return StepLimit;

#ifdef L4LOG
       G4cout 
             << "L4Cerenkov::PostStepGetPhysicalInteractionLength"
             << "OK.3 :  gamma >= GammaMin "
             << " gamma " << gamma 
             << " GammaMin " << GammaMin 
             << G4endl
             ; 
#endif 




        G4double kinEmin = mass*(GammaMin-1.);

        G4double RangeMin = G4LossTableManager::Instance()->
                                                   GetRange(particleType,
                                                            kinEmin,
                                                            couple);
        G4double Range    = G4LossTableManager::Instance()->
                                                   GetRange(particleType,
                                                            kineticEnergy,
                                                            couple);

        G4double Step = Range - RangeMin;

#ifdef L4LOG
       G4cout 
             << "L4Cerenkov::PostStepGetPhysicalInteractionLength"
             << "??.4 : Step > 1um "
             << " Step " << Step 
             << " Range " << Range  
             << " kineticEnergy " << kineticEnergy
             << " RangeMin " << RangeMin  
             << " kinEmin " << kinEmin
             << G4endl
             ; 
#endif 


        if (Step < 1.*CLHEP::um ) return StepLimit;


#ifdef L4LOG
       G4cout 
             << "L4Cerenkov::PostStepGetPhysicalInteractionLength"
             << "OK.4 : Step > 1um "
             << " Step " << Step 
             << " Range " << Range  
             << " RangeMin " << RangeMin  
             << G4endl
             ; 
#endif 




        if (Step > 0. && Step < StepLimit) StepLimit = Step; 

        // If user has defined an average maximum number of photons to
        // be generated in a Step, then calculate the Step length for
        // that number of photons. 
 
        if (fMaxPhotons > 0) {

           // particle charge
           const G4double charge = aParticle->
                                   GetDefinition()->GetPDGCharge();

	   G4double MeanNumberOfPhotons = 
                    GetAverageNumberOfPhotons(charge,beta,aMaterial,Rindex);

           Step = 0.;
           if (MeanNumberOfPhotons > 0.0) Step = fMaxPhotons /
                                                 MeanNumberOfPhotons;

           if (Step > 0. && Step < StepLimit) StepLimit = Step;
        }

        // If user has defined an maximum allowed change in beta per step
        if (fMaxBetaChange > 0.) {

           G4double dedx = G4LossTableManager::Instance()->
                                                   GetDEDX(particleType,
                                                           kineticEnergy,
                                                           couple);

           G4double deltaGamma = gamma - 
                                 1./std::sqrt(1.-beta*beta*
                                                 (1.-fMaxBetaChange)*
                                                 (1.-fMaxBetaChange));

           Step = mass * deltaGamma / dedx;

           if (Step > 0. && Step < StepLimit) StepLimit = Step;

        }

        *condition = StronglyForced;
        return StepLimit;
}
