#include "TrackingAction.hh"
#include "G4OpticksRecorder.hh"

TrackingAction::TrackingAction(G4OpticksRecorder* okr_)
    :
    okr(okr_)
{
}

void TrackingAction::PreUserTrackingAction(const G4Track* track)
{
    okr->PreUserTrackingAction(track); 
}
void TrackingAction::PostUserTrackingAction(const G4Track* track)
{
    okr->PostUserTrackingAction(track);     
}


