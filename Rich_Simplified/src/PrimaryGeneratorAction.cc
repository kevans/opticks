#include "PrimaryGeneratorAction.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleGun.hh"

#include "G4Opticks.hh"





using CLHEP::MeV ; 

PrimaryGeneratorAction::PrimaryGeneratorAction()
    :
    G4VUserPrimaryGeneratorAction(),
    fParticleGun(new G4ParticleGun(1))
{
    G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
    G4ParticleDefinition* particle = particleTable->FindParticle("e+");
    fParticleGun->SetParticleDefinition(particle);
    fParticleGun->SetParticleTime(0.0*CLHEP::ns);
    fParticleGun->SetParticlePosition(G4ThreeVector(0.0*CLHEP::cm,0.0*CLHEP::cm,0.0*CLHEP::mm));
    fParticleGun->SetParticleMomentumDirection(G4ThreeVector(0.,0.1,1.));
    fParticleGun->SetParticleEnergy(4000.*MeV);   // few photons at ~0.7*MeV loads from ~ 0.8*MeV
}

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
    fParticleGun->GeneratePrimaryVertex(anEvent);

    // not strictly required by Opticks, but useful for debugging + visualization of non-opticals
    //G4Opticks::Get()->collectPrimaries(anEvent) ;  

}


 
