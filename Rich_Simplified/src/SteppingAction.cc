#include "SteppingAction.hh"
#include "G4Track.hh"
#include "G4Step.hh"
#include "G4OpticksRecorder.hh"

SteppingAction::SteppingAction(G4OpticksRecorder* okr_)
    :
    okr(okr_)
{
}

void SteppingAction::UserSteppingAction(const G4Step* step)
{
    okr->UserSteppingAction(step); 
}



