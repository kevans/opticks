import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

'''
#default: use the original option with deltatheta deltaphi
opticks_hits = open("Opticks_hits.txt","r")
hits = []
for line in opticks_hits:
    hits.append( [float(s) for s in line.strip().split()] )
hits = np.array(hits)
#from Sajan: use the option of having an exact flat mirror
opticks_hits_exactflat = open("Opticks_hits_exactflat.txt","r")
hits_exactflat = []
for line in opticks_hits_exactflat:
    hits_exactflat.append( [float(s) for s in line.strip().split()] )
hits_exactflat = np.array(hits_exactflat)
'''
#from Simon: use the option with single deltaTheta and box substractions for test
opticks_hits_substraction = open("Opticks_hits_substraction.txt","r")
hits_substraction = []
for line in opticks_hits_substraction:
    hits_substraction.append( [float(s) for s in line.strip().split()] )
hits_substraction = np.array(hits_substraction)

g4_hits = open("Geant4_hits.txt","r")
hits_g4 = []
for line in g4_hits:
    hits_g4.append( [float(s) for s in line.strip().split()] )
hits_g4 = np.array(hits_g4)

#plt.scatter(hits[:,0],hits[:,1],s=5,color='black',label='Deltatheta deltaphi: '+str(hits.shape[0])+' hits')
#plt.scatter(hits_exactflat[:,0],hits_exactflat[:,1],s=5,color='red',label='Exact flat mirror: '+str(hits_exact.shape[0])+' hits')
plt.scatter(hits_substraction[:,0],hits_substraction[:,1],s=5,color='blue',label='Box substractions: '+str(hits_substraction.shape[0])+' hits')
plt.scatter(hits_g4[:,0],hits_g4[:,1],s=5,color='green',label='Geant4: '+str(hits_g4.shape[0])+' hits')
plt.legend(loc='best')

plt.title("Hits collected by Opticks")
plt.xlabel("x")
plt.ylabel("y")

plt.savefig("draw_GeometryTest.pdf")
plt.show()
    
