#pragma once
#include "SArgs.hh"

class G4RunManager;
class G4VPhysicalVolume;

class G4Opticks;
struct G4OpticksRecorder;

struct RunAction;
struct EventAction;
struct TrackingAction;
struct SteppingAction;

struct RichTbOpticksDetectorConstruction;
class PhysicsList;
//struct RichTbPhysicsList;
class RichScintillation;

//struct RichTbPhysicsList;
struct PrimaryGeneratorAction;

struct RichTbSimH
{
    RichTbSimH( int argc, char** argv, const char* argforced );
    ~RichTbSimH();

    void init();
    double beamOn(int num_ev); 
    void Finalize();

    G4Opticks* g4ok;
    G4OpticksRecorder* okr;
    G4RunManager*	rm;
    RichTbOpticksDetectorConstruction*	dc;
    //RichTbPhysicsList*	pl;
    PhysicsList*	pl;
    PrimaryGeneratorAction*	ga;
    SArgs* m_sargs;

    RunAction*   ra;
    EventAction* ea;
    TrackingAction* ta;
    SteppingAction*  sa;


};
