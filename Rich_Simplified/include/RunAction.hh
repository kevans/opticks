#pragma once

#include "G4UserRunAction.hh"
#include <ctime>
#include "G4OpticksRecorder.hh"

struct G4OpticksRecorder ; 

struct RunAction : public G4UserRunAction
{
    RunAction(G4OpticksRecorder* okr); 
    virtual void BeginOfRunAction(const G4Run* run);
    virtual void EndOfRunAction(const G4Run* run);
    G4OpticksRecorder*       okr ; 
    clock_t startTime, endTime;
    double getTime();
};
