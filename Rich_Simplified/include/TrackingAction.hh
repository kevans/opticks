#pragma once

#include "G4UserTrackingAction.hh"

class G4Track ; 
struct G4OpticksRecorder ; 

struct TrackingAction : public G4UserTrackingAction
{
    TrackingAction(G4OpticksRecorder* okr_); 
    virtual void PreUserTrackingAction(const G4Track* track);
    virtual void PostUserTrackingAction(const G4Track* track);
    G4OpticksRecorder*  okr ; 
};
