/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef RichPmtQuantEff_h
#define RichPmtQuantEff_h 1

#include <map>


class RichPmtQuantEff{
 public:
  RichPmtQuantEff();

  // RichPmtQuantEff(int pmtnum, int richdetnum); //This line may be activated for having a table per pmt.

  virtual ~RichPmtQuantEff() {};
  static RichPmtQuantEff* getRichPmtQuantEffInstance();

  void SetPmtQeTab(std::map<double,double> aTab) {m_PmtQeTable = aTab ;}
  double getPmtQEffFromPhotEnergy(double  photonenergy );
  void UpdatePmtQETable(double aEn, double aQE);
  void FillStdPmtQEInfo();


 private:

  std::map<double,double> m_PmtQeTable;

  static RichPmtQuantEff*  RichPmtQuantEffInstance;


};

#endif //end of RichPmtQuantEff_h
