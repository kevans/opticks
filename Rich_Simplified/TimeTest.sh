#!/bin/bash -l 

export X4PhysicalVolume=INFO
export GMeshLib=INFO

export WITH_OPTICKS_mode=1
export WITH_GEANT4_mode=0
if [ -n "$DEBUG" ]; then 
    lldb__ TimeTest_Opticks
else
    TimeTest
fi  

export WITH_OPTICKS_mode=0
export WITH_GEANT4_mode=1
if [ -n "$DEBUG" ]; then 
    lldb__ TimeTest_Geant4
else
    TimeTest
fi 

python ../draw_TimeTest.py 
export WITH_OPTICKS_mode=1
