import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

#pure opticks workflow
opticks_time = open("Opticks_time.txt","r")
op_time = []
for line in opticks_time:
    op_time.append( [float(s) for s in line.strip().split()] )
op_time = np.array(op_time)
#pure Geant4 workflow
geant4_time = open("Geant4_time.txt","r")
g4_time = []
for line in geant4_time:
    g4_time.append( [float(s) for s in line.strip().split()] )
g4_time = np.array(g4_time)

plt.scatter(op_time[:,0],op_time[:,1],s=5,color='red',label='Opticks')
plt.scatter(g4_time[:,0],g4_time[:,1],s=5,color='blue',label='Geant4')
plt.legend(loc='best')

plt.plot(op_time[:,0],op_time[:,1],color='red')
plt.plot(g4_time[:,0],g4_time[:,1],color='blue')
plt.xlabel("Number of events")
plt.ylabel("Times(s)")

plt.savefig("draw_TimeTest.pdf")
#plt.show()
    
